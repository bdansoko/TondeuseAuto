package challenge.tondeuse.modele;

import challenge.surface.modele.Surface;

import java.util.ArrayList;
import java.util.List;

public abstract class Tondeuse {
    protected static final List<Tondeuse> listTondeuses= new ArrayList<>();

	/**
	 * Permet d'exécuter une séquence d'instructions
	 * @param action : String
	 */
	public abstract void executerSequence(String action);

    /**
     * Renvoie la position de la tondeuse
     * @return Position
     */
    public abstract Position getPosition();

    /**
     * Modifie la position de la tondeuse
     * @param position : Position
     */
    public abstract void setPosition(Position position);

    /**
     * retourne la pelouse associee à la tondeuse
     * @return  pelouse : Surface
     */
    public abstract Surface getPelouse();

    /**
     * Associe la tondeuse à la pelouse
     * @param pelouse : Pelouse
     */
    public abstract void setPelouse(Surface pelouse);

    /**
     * Nombre total de tondeuses
     * @return : int
     */
    public static int getTotal(){
        return listTondeuses.size();
    }

    /**
     * Supprimer une tondeuse
     * @param t : Tondeuse
     */
    public static void delete(Tondeuse t){
        if (listTondeuses.contains(t)) {
            int index = listTondeuses.indexOf(t);
            listTondeuses.remove(index);
        }
    }

    /**
     * Test si une tondeuse existe à la position
     * @param p : Position
     * @return : boolean
     */
    public static boolean existAtPosition(Position p){
        for (Tondeuse t : listTondeuses){
            if (t.getPosition().equals(p)){
                return true;
            }
        }
        return false;
    }

    /**
     *  Renvoie la liste de tondeuses
     * @return List
     */
    public static List<Tondeuse> getListTondeuses() {
        return listTondeuses;
    }
}