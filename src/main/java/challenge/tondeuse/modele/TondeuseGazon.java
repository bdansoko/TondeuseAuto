package challenge.tondeuse.modele;


import challenge.surface.modele.Surface;
import challenge.surface.modele.SurfaceRectangulaire;
import challenge.utils.Constantes;
import org.apache.log4j.Logger;
import java.util.ArrayList;
import java.util.List;


/**
 * Cette classe permet de créer une tondeuse, de definir sa position et l'associer à une pelouse
 * La tondeuse peut exécuter une sequence d'instructions, communiquer sa position et la pelouse quelle tond
 * @author bdansoko
 */
public class TondeuseGazon extends Tondeuse {

    //Logger de classe
	 private static final Logger logger = Logger.getLogger(TondeuseGazon.class);

    /**
     * Position de la tondeuse
     */
	private Position position;

    /**
     * La pelouse associée
     */
	private Surface pelouse;

    /**
     * Constructeur
     * @param position : Position
     */
	public TondeuseGazon(Position position) {
		this.position = position;
		if (!Tondeuse.listTondeuses.contains(this)) {
            Tondeuse.listTondeuses.add(this);
        }
	}

    /**
     * Constructeur
     * @param position : Position
     * @param pelouse : SurfaceRectangulaire
     */
    public TondeuseGazon(Position position,SurfaceRectangulaire pelouse){
        this.pelouse = pelouse;
        this.position = position;
        if (!Tondeuse.listTondeuses.contains(this)) {
            Tondeuse.listTondeuses.add(this);
        }
    }

    /**
     * Constructeur
     * @param pelouse : SurfaceRectangulaire
     */
    public TondeuseGazon(SurfaceRectangulaire pelouse){
        this.pelouse = pelouse;
        if (!Tondeuse.listTondeuses.contains(this)) {
            Tondeuse.listTondeuses.add(this);
        }
    }

    /**
     * Constructeur par defaut
     */
    public TondeuseGazon(){
        if (!Tondeuse.listTondeuses.contains(this)) {
            Tondeuse.listTondeuses.add(this);
        }
    }

    /**
     * Renvoie la position de la tondeuse
     * @return Position
     */
	public Position getPosition() {
        int index = listTondeuses.indexOf(this);
        return ((TondeuseGazon)Tondeuse.listTondeuses.get(index)).position;
	}

    /**
     * Modifie la position de la tondeuse
     * @param position : Position
     */
	public void setPosition(Position position) {
		int index = listTondeuses.indexOf(this);
        ((TondeuseGazon)Tondeuse.listTondeuses.get(index)).position = position;
	}

    /**
     * retourne la pelouse associee à la tondeuse
     * @return  pelouse : Surface
     */
    public Surface getPelouse() {
        int index = listTondeuses.indexOf(this);
        return ((TondeuseGazon)Tondeuse.listTondeuses.get(index)).pelouse ;
    }

    /**
     * Associe la tondeuse à la pelouse
     * @param pelouse : Pelouse
     */
	public void setPelouse(Surface pelouse) {
        int index = listTondeuses.indexOf(this);
        ((TondeuseGazon)Tondeuse.listTondeuses.get(index)).pelouse = pelouse;

	}

    /**
     * Transforme une sequence de lettre en une liste d'action
     * @param sequence : String
     * @return List
     */
	public List<Action> stringToActionArray(String sequence){
	    ArrayList<Action> res =  new ArrayList<>();
        char[] tab = sequence.toCharArray();
        for (char lettre : tab){
            switch (String.valueOf(lettre)){
                case Constantes.DROITE:
                    res.add(Action.DROITE);
                    break;
                case Constantes.GAUCHE:
                    res.add(Action.GAUCHE);
                    break;
                case Constantes.AVANCER:
                    res.add(Action.AVANCER);
                    break;
                default:
                    break;
            }
        }
        return res;
    }

    /**
     * Permet d'exécuter une sequence d'instruction
     * @param sequence : String
     */
	public synchronized void executerSequence(String sequence) {
	    logger.debug("Exécution de la sequence: " + sequence);
        List<Action> actionsArray = stringToActionArray(sequence);

        int index = listTondeuses.indexOf(this);

        for (Action action : actionsArray){
			logger.info("Exécution de l'action: " + action.toString());
			switch (action.toString()){
				case Constantes.DROITE:
				    executeActionDroite(index);
						break;
				case Constantes.GAUCHE:
				    executeActionGauche(index);
						break;
				case Constantes.AVANCER:
				    executeActionAvancer(index);
				    break;
				default:
                    logger.info("Action inconnue");
                    logger.info(Constantes.POSITIONLOG + ((TondeuseGazon)Tondeuse.listTondeuses.get(index)).position.toString());
				  break;
			}
		}
		
	}

	public void executeActionDroite(int index){
        switch (((TondeuseGazon)Tondeuse.listTondeuses.get(index)).position.getOrientation().toString()) {
            case Constantes.NORTH:
                ((TondeuseGazon)Tondeuse.listTondeuses.get(index)).position.setOrientation(Orientation.EAST);
                logger.info(Constantes.POSITIONLOG + ((TondeuseGazon)Tondeuse.listTondeuses.get(index)).position.toString());
                break;
            case Constantes.SOUTH:
                ((TondeuseGazon)Tondeuse.listTondeuses.get(index)).position.setOrientation(Orientation.WEST);
                logger.info(Constantes.POSITIONLOG + ((TondeuseGazon)Tondeuse.listTondeuses.get(index)).position.toString());
                break;
            case Constantes.WEST:
                ((TondeuseGazon)Tondeuse.listTondeuses.get(index)).position.setOrientation(Orientation.NORTH);
                logger.info(Constantes.POSITIONLOG + ((TondeuseGazon)Tondeuse.listTondeuses.get(index)).position.toString());
                break;
            case Constantes.EAST:
                ((TondeuseGazon)Tondeuse.listTondeuses.get(index)).position.setOrientation(Orientation.SOUTH);
                logger.info(Constantes.POSITIONLOG + ((TondeuseGazon)Tondeuse.listTondeuses.get(index)).position.toString());
                break;

            default:
                break;
        }
    }

    public void executeActionGauche(int index){
        switch (((TondeuseGazon)Tondeuse.listTondeuses.get(index)).position.getOrientation().toString()) {
            case Constantes.NORTH:
                ((TondeuseGazon)Tondeuse.listTondeuses.get(index)).position.setOrientation(Orientation.WEST);
                logger.info(Constantes.POSITIONLOG +((TondeuseGazon)Tondeuse.listTondeuses.get(index)).position.toString());
                break;
            case Constantes.SOUTH:
                ((TondeuseGazon)Tondeuse.listTondeuses.get(index)).position.setOrientation(Orientation.EAST);
                logger.info(Constantes.POSITIONLOG + ((TondeuseGazon)Tondeuse.listTondeuses.get(index)).position.toString());
                break;
            case Constantes.WEST:
                ((TondeuseGazon)Tondeuse.listTondeuses.get(index)).position.setOrientation(Orientation.SOUTH);
                logger.info(Constantes.POSITIONLOG + ((TondeuseGazon)Tondeuse.listTondeuses.get(index)).position.toString());
                break;
            case Constantes.EAST:
                ((TondeuseGazon)Tondeuse.listTondeuses.get(index)).position.setOrientation(Orientation.NORTH);
                logger.info(Constantes.POSITIONLOG + ((TondeuseGazon)Tondeuse.listTondeuses.get(index)).position.toString());
                break;
            default:
                break;
        }
    }

    public void executeActionAvancer(int index){
        switch (((TondeuseGazon)Tondeuse.listTondeuses.get(index)).position.getOrientation().toString()) {
            case Constantes.NORTH:
                if (((TondeuseGazon)Tondeuse.listTondeuses.get(index)).position.getCoordonnees().getY()+1 <= pelouse.getCoteY()){
                    ((TondeuseGazon)Tondeuse.listTondeuses.get(index)).position.setCoordonneeY(1);
                    logger.info(Constantes.POSITIONLOG + ((TondeuseGazon)Tondeuse.listTondeuses.get(index)).position.toString());
                }
                break;
            case Constantes.SOUTH:
                if (((TondeuseGazon)Tondeuse.listTondeuses.get(index)).position.getCoordonnees().getY()-1 >= 0) {
                    ((TondeuseGazon)Tondeuse.listTondeuses.get(index)).position.setCoordonneeY(-1);
                    logger.info(Constantes.POSITIONLOG + ((TondeuseGazon)Tondeuse.listTondeuses.get(index)).position.toString());
                }
                break;
            case Constantes.WEST:
                if (((TondeuseGazon)Tondeuse.listTondeuses.get(index)).position.getCoordonnees().getX()-1 >= 0){
                    ((TondeuseGazon)Tondeuse.listTondeuses.get(index)).position.setCoordonneeX(-1);
                    logger.info(Constantes.POSITIONLOG + ((TondeuseGazon)Tondeuse.listTondeuses.get(index)).position.toString());
                }
                break;
            case Constantes.EAST:
                if (((TondeuseGazon)Tondeuse.listTondeuses.get(index)).position.getCoordonnees().getX()+1 <= pelouse.getCoteX()) {
                    ((TondeuseGazon)Tondeuse.listTondeuses.get(index)).position.setCoordonneeX(1);
                    logger.info(Constantes.POSITIONLOG + ((TondeuseGazon)Tondeuse.listTondeuses.get(index)).position.toString());
                }
                break;
            default:
                break;
        }
    }

}
