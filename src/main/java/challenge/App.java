package challenge;

import challenge.surface.modele.Surface;
import challenge.surface.modele.SurfaceRectangulaire;
import challenge.tondeuse.modele.Position;
import challenge.tondeuse.modele.Tondeuse;
import challenge.tondeuse.modele.TondeuseGazon;
import challenge.utils.OsHelper;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import org.apache.log4j.Logger;

public class App 
{
    //Logger de classe
    private static final  Logger logger = org.apache.log4j.Logger.getLogger(App.class);

    static Surface pelouse = null;
    static Tondeuse tondeuse = null;

    public static void main( String[] args )
    {
        controlerParametre(args);
        logger.info("Debut du traitement");
        OsHelper osHelper = new OsHelper();
        try(BufferedReader br = new BufferedReader(new FileReader(osHelper.toFile(args[0])))){ //je cree le reader du fichier
            String line;
            while ((line = br.readLine()) != null) { // je lis le fichier ligne par ligne
                if (line.trim().contains(" ")){ // Si la ligne contient des espaces
                    String[] tab = line.split(" ");
                    if (tab.length == 2){// si c'est la dimension de la pelouse
                        pelouse = new SurfaceRectangulaire(Integer.parseInt(tab[0]),Integer.parseInt(tab[1]));//je la crée
                        logger.info("Création de la surface avec les dimensions: x=" + line);
                    }else if(tab.length == 3){ //si c'est la position de la tondeuse
                        setPosition(tab,line);
                    }
                }else{ // Si c'est une sequence d'actions
                    executerActions(line);
                }
            }
        } catch (IOException e) {
            logger.error(e);
        }
    }

    private static void setPosition(String[] tab, String line){
        Position position = new Position(Integer.parseInt(tab[0]), Integer.parseInt(tab[1]), tab[2]); //Position de la tondeuse
        Tondeuse.delete(tondeuse); // je supprime l'ancienne tondeuse
        tondeuse = new TondeuseGazon(position);//je crée la nouvelle
        tondeuse.setPelouse(pelouse); //J'associe la tondeuse à la pelouse
        logger.info("Création de la tondeuse à la position: " + line);
    }

    private static void executerActions(String line){
        if (pelouse == null || tondeuse == null){ //Si la pelouse ou la tondeuse n'existe pas
            logger.error("le fichier contient des erreurs. La surface ou la position de la tondeuse n'ont pas été renseignées");
        }else{
            tondeuse.executerSequence(line);
        }
    }

    private static void controlerParametre(String[] arg){
        if (arg.length !=  1){
            logger.error("Nombre de paramètres invalide. Il faut renseigner le nom absolu du fichier de données");
            System.exit (0);
        }
        OsHelper osHelper = new OsHelper();
        if (!osHelper.exist(arg[0])){
            logger.error("Le fichier passé en paramètre n'existe pas");
            System.exit (0);
        }
    }

}
