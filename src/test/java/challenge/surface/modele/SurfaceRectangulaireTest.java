package challenge.surface.modele;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class SurfaceRectangulaireTest {

    SurfaceRectangulaire pelouse;
    @Before
    public void setUp() throws Exception {
        pelouse = new SurfaceRectangulaire(5,5);
    }

    @Test
    public void getCoteX() {
        Assert.assertEquals(5,pelouse.getCoteX());
    }

    @Test
    public void getCoteY() {
        Assert.assertEquals(5,pelouse.getCoteY());
    }

    @Test
    public void toStringFormat() {
        Assert.assertEquals("5 5",pelouse.toString());
    }
}