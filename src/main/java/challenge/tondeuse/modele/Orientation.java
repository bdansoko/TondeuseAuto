package challenge.tondeuse.modele;

/**
 * lettre indiquant l'orientation de la tondeuse selon la notation cardinale anglaise (N,E,W,S)
 * @author bdansoko
 *
 */
public enum Orientation {
	//Instantiation des enumeration en utilisant son constructeur privé
	NORTH("N"),
	EAST("E"),
	WEST("W"),
	SOUTH("S");
	
	/**
	 * Symbol de l'orientation N,E,W ou S
	 */
	private String symbol;
	
	/**
	 * Constructeur privé
	 * @param symbol
	 */
	private Orientation(String symbol){
		this.symbol = symbol;
	}
	
	/**
	 * permet d'afficher le symbol de l'orientation
	 */
	@Override
	public String toString(){
		return symbol;
	}

	public boolean egale(Orientation o){
		return this.symbol.equals(o.symbol);
	}

}
