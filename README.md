# Tondeuse automatique

## 1. Prérequis

Ce projet est un Maven Project. Il faut soit avoir un `Maven Embedded` ou installer un Maven en local.
- La version __Maven__ est la __3.3.9__
- La version __JDK__ utilisée est la __1.8__
- L'__IDE__ utilisé pour la création du projet est __Intellij__
 
## 2. Execution:
### 2.1 Jar
Les jars sont générés dans le dossier `target`, à la racine.
  - __`mvn package`__ pour générer le une fichier (__`tondeuse-1.0-SNAPSHOT-jar-with-dependencies.jar`__) dans le dossier `target`.<br/> 
  
  Pour exécuter le Jar il faut:
  - Ouvrir l'interpreteur de commande windows
  - Se placer à la racine du projet
  - Exécuter la commande:<br/>
 ```bash
 java -jar target\tondeuse-1.0-SNAPSHOT-jar-with-dependencies.jar <AbsoluteFileName>
 ```
 __`<AbsoluteFileName>`__ est à remplacer par le nom absolu du fichier de commandes. Ce fichier se trouve dans le dossier `input` du projet  
 
### 2.2 Test
 Dans la partie test, vous y trouverez les tests unitaires `junit`, les steps et les features Cucumber.
 
## 3. Documentation
### 3.1 Diagramme de classe:
 Le diagramme de classe se trouve dans le dossier `TondeuseAuto\docs\diagrammes`
### 3.2 Javadoc
 La javadoc se trouve dans le dossier `TondeuseAuto\docs\javadoc`
 
## 4. Log
 j'ai utilisé la version 1.2.17 de log4j. Pour adapter la generation du fichier de log, il est impératif d'adapter le fichier `log4j.properties` dans le dossier `src\main\java\ressources`.
 Le niveau de log est `Debug` et la sortie standard est celle du système.
 Vous pouvez aussi rediriger la sortie vers un fichier en rajoutant l'option file comme ceci
 __``log4j.rootLogger=DEBUG, stdout, file``__. Par défaut la sortie file correspond au fichier __`C:\\log4j-application.log`__. Vous pouvez aussi modifier cela.
 
## 5. Qualité de code
La qualité de code est vérifiée avec `__Sonar__`. J'ai utilisé le plugin `__SonarLint__` sur Intellij.
