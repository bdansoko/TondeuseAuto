package challenge.utils;

public final class Constantes {

	private Constantes(){}

	//Orientations
	public static final String NORTH = "N";
	public static final String EAST = "E";
	public static final String WEST = "W";
	public static final String SOUTH = "S";

	//Actions
	public static final String AVANCER = "A";
	public static final String DROITE = "D";
	public static final String GAUCHE = "G";

	//Logs
	public static final String POSITIONLOG = "Position de la tondeuse: ";
}
