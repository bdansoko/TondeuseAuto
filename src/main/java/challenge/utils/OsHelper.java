package challenge.utils;

import java.io.File;

public class OsHelper {

    /**
     * Retourn true si le fichier existe, false sinon
     * @param path : String
     * @return boolean
     */
    public boolean exist(String path){
        File f = new File(path);
        return f.exists();
    }

    /**
     * retourne un objet File
     * @param path : String
     * @return java.io.File
     */
    public File toFile(String path){
        return (new File(path));
    }

}
