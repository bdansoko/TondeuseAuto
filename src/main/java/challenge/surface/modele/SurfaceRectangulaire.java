package challenge.surface.modele;

import java.util.Objects;

/**
 * C'est la surface à tondre. 
 * @author bdansoko
 *
 */
public class SurfaceRectangulaire implements Surface{
	/**
	 * Cote equivalent à l'abscisse
	 */
	private int coteX;
	/**
	 * Cote correspondant à l'ordonnee
	 */
	private int coteY;
	
	/**
	 * Constructeur
	 * @param coteX : int
	 * @param coteY : int
	 */
	public SurfaceRectangulaire(int coteX, int coteY) {
		this.coteX = coteX;
		this.coteY = coteY;
	}

	/**
	 * Permet d'acccéder  a la valeur de l'abcisse
	 * @return int
	 */
	public int getCoteX() {
		return coteX;
	}


	/**
	 * Permet d'acccéder a la valeur de l'ordonnee
	 * @return int
	 */
	public int getCoteY() {
		return coteY;
	}

	/**
	 * Renvoi une chaine de caractère spécifique correspondante à l'état de la pelouse
	 * @return String
	 */
	public String toString() {
		return coteX + " " + coteY;
	}

	public boolean equals(Object c){
	    if (c instanceof Surface) {
	        return this.coteX==((Surface) c).getCoteX() && this.coteY==((Surface) c).getCoteY();
        }else{
	        return false;
        }
    }

	@Override
	public int hashCode() {

		return Objects.hash(coteX, coteY);
	}
}
