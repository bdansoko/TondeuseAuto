package challenge.utils;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.*;

public class OsHelperTest {

    String fileName;
    OsHelper osHelper;

    @Before
    public void setUp() throws Exception {
        fileName = "D:\\unitTest.txt";
        osHelper = new OsHelper();
        PrintWriter out = new PrintWriter(new FileWriter(fileName));
        out.close();
    }

    @After
    public void tearDown() throws Exception {
        File fichier = new File(fileName);
        fichier.delete();
    }

    @Test
    public void exist() {
        Assert.assertTrue(osHelper.exist(fileName));
    }

    @Test
    public void toFile() {
        File f = osHelper.toFile(fileName);
        Assert.assertTrue(f.isFile());
        f.delete();
    }
}