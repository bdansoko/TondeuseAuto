package challenge.tondeuse.modele;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CoordonneesTest {
    Coordonnees c ;
    @Before
    public void setUp() throws Exception {
        c = new Coordonnees(1,2);
    }

    @Test
    public void getX() {
        Assert.assertEquals(1,c.getX());
    }

    @Test
    public void setX() {
        c.setX(2);
        Assert.assertEquals(2,c.getX());
    }

    @Test
    public void getY() {
        Assert.assertEquals(2,c.getY());
    }

    @Test
    public void setY() {
        c.setY(3);
        Assert.assertEquals(3,c.getY());
    }

    @Test
    public void toStringFormat() {
        Assert.assertEquals("1 2",c.toString());
    }
}