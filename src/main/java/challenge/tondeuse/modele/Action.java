package challenge.tondeuse.modele;

/**
 * Pour contrôler la tondeuse, on lui envoie une séquence simple de lettres. Les lettres possibles
 * sont « D », « G » et « A ». « D » et « G » font pivoter la tondeuse de 90° à droite ou à gauche
 * respectivement, sans la déplacer. « A » signifie que l'on avance la tondeuse d'une case dans la
 * direction à laquelle elle fait face, et sans modifier son orientation
 * @author bdansoko
 *
 */

	public enum Action {
	
		AVANCER("A"),
		DROITE("D"),
		GAUCHE("G");
		/**
		 * Correspond à « D », « G » ou « A »
		 */
		private String lettre;
		
		/**
		 * Constructeur
		 * @param lettre : String
		 */
		private Action(String lettre){
			this.lettre = lettre;
		}
		
		/**
		 * Renvoie la lettre
		 */
		@Override
		public String toString(){
			return lettre;
		}
	}