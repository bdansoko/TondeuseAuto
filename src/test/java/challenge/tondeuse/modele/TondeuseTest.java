package challenge.tondeuse.modele;

import challenge.surface.modele.Surface;
import challenge.surface.modele.SurfaceRectangulaire;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TondeuseTest {

    Surface pelouse;
    Position positionA;
    Tondeuse tondeuseA;
    Position positionB;
    Tondeuse tondeuseB;


    @Before
    public void setUp() throws Exception {
        Tondeuse.listTondeuses.clear();
        pelouse = new SurfaceRectangulaire(5,5);
        positionA = new Position(1, 2, Orientation.NORTH);
        tondeuseA = new TondeuseGazon(positionA);
        tondeuseA.setPelouse(pelouse);
        positionB = new Position(3, 3, Orientation.EAST);
        tondeuseB = new TondeuseGazon(positionB);
        tondeuseB.setPelouse(pelouse);
    }

   @Test
    public void existAtPosition() {
        Assert.assertTrue(Tondeuse.existAtPosition(positionA));
        Assert.assertTrue(Tondeuse.existAtPosition(positionB));
        Position positionC = new Position(3, 3, Orientation.NORTH);
        Assert.assertFalse(Tondeuse.existAtPosition(positionC));

    }

    @Test
    public void getTotal() {
        for (Tondeuse t :Tondeuse.listTondeuses){
            System.out.println(t.getPosition());
        }
    }

    @Test
    public void delete() {
        Tondeuse.delete(tondeuseA);
        Assert.assertEquals(1,Tondeuse.getTotal());
        Assert.assertFalse(Tondeuse.listTondeuses.contains(tondeuseA));
    }
}