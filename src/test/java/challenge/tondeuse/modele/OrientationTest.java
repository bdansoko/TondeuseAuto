package challenge.tondeuse.modele;

import challenge.utils.Constantes;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class OrientationTest {

    Orientation o;

    @Before
    public void setUp() throws Exception {
        o = Orientation.EAST;
    }

    @Test
    public void getValue() {
        Assert.assertEquals(Constantes.EAST,o.toString());
    }
}