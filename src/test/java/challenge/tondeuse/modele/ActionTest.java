package challenge.tondeuse.modele;

import challenge.utils.Constantes;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ActionTest {

    Action a ;

    @Before
    public void setUp() throws Exception {
        a = Action.AVANCER;
    }

    @Test
    public void toStringFormat() {
        Assert.assertEquals(a.toString(),String.valueOf(Constantes.AVANCER));
    }
}