package challenge.tondeuse.modele;

import java.util.Objects;

/**
 * Coordonnees de la tondeuse représentées par une combinaison (x,y) 
 * @author bdansoko
 *
 */
public class Coordonnees {

	/**
	 * Coordonnee x
	 */
	private int x;
	
	/**
	 * Coordonnee y
	 */
	private int y;

	/**
	 * Constucteur 
	 * @param x : int
	 * @param y : int
	 */
	public Coordonnees(int x, int y) {
		this.x = x;
		this.y = y;
	}

	/**
	 * Permet d'acceder au contenu de la coordonnee x
	 * @return int
	 */
	public int getX() {
		return x;
	}

	/**
	 * Permet de modifier la coordonnee x
	 * @param x : int
	 */
	public void setX(int x) {
		this.x = x;
	}

	/**
	 * Permet d'acceder au contenu de la coordonnee y
	 * @return int
	 */
	public int getY() {
		return y;
	}

	/**
	 * Permet de modifier la coordonnee y
	 * @param y : int
	 */
	public void setY(int y) {
		this.y = y;
	}

	/**
	 * Retourne une String representant les coordonnees de la tondeuse 
	 */
	public String toString() {
		return  x + " " + y;
	}

	public boolean equals(Object c) {
		if (c instanceof Coordonnees) {
			return this.getX()==((Coordonnees)c).getX() && this.getY()==((Coordonnees)c).getY();
		}else
			return false;
	}

    @Override
    public int hashCode() {

        return Objects.hash(x, y);
    }
}
