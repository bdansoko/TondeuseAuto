package challenge.tondeuse.modele;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class PositionTest {

    Position positionA;

    @Before
    public void setUp() throws Exception {
        positionA = new Position(1, 2, Orientation.NORTH);
    }

    @Test
    public void getCoordonnees() {
        Coordonnees a = new Coordonnees(1,2);
        Assert.assertTrue(positionA.getCoordonnees().equals(a));
    }

    @Test
    public void setCoordonnees() {
        Coordonnees a = new Coordonnees(2,2);
        positionA.setCoordonnees(2,2);
        Assert.assertTrue(positionA.getCoordonnees().equals(a));
    }

    @Test
    public void setCoordonneeX() {
        Coordonnees a = new Coordonnees(2,2);
        positionA.setCoordonneeX(1);
        Assert.assertTrue(positionA.getCoordonnees().equals(a));
    }

    @Test
    public void setCoordonneeY() {
        Coordonnees a = new Coordonnees(1,3);
        positionA.setCoordonneeY(1);
        Assert.assertTrue(positionA.getCoordonnees().equals(a));
    }

    @Test
    public void getOrientation() {
        Orientation o = Orientation.NORTH;
        Assert.assertEquals(positionA.getOrientation(),o);
    }

    @Test
    public void setOrientation() {
        Orientation o = Orientation.EAST;
        positionA.setOrientation(Orientation.EAST);
        Assert.assertEquals(positionA.getOrientation(),o);
    }

    @Test
    public void toStringFormat() {
        Orientation o = Orientation.NORTH;
        Coordonnees a = new Coordonnees(1,2);
        Position p = new Position(a,o);
        Assert.assertTrue(positionA.equals(p));
    }
}