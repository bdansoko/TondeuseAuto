package challenge.tondeuse.modele;

import challenge.surface.modele.Surface;
import challenge.surface.modele.SurfaceRectangulaire;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TondeuseGazonTest{

    TondeuseGazon tondeuseA;
    Position positionA;
    Surface pelouse;

    @Before
    public void setUp() throws Exception {
        pelouse = new SurfaceRectangulaire(5,5);
        positionA = new Position(1, 2, Orientation.NORTH);
        tondeuseA = new TondeuseGazon(positionA);
        tondeuseA.setPelouse(pelouse);
    }

    @Test
    public void getPosition() {
        Assert.assertTrue(tondeuseA.getPosition().equals(positionA));
    }

    @Test
    public void setPosition() {
        positionA = new Position(2, 3, Orientation.SOUTH);
        tondeuseA.setPosition(new Position(2, 3, Orientation.SOUTH));
        Assert.assertTrue(tondeuseA.getPosition().equals(positionA));
    }

    @Test
    public void setPelouse() {
        pelouse = new SurfaceRectangulaire(6,6);
        tondeuseA.setPelouse(pelouse);
        Assert.assertTrue(tondeuseA.getPelouse().equals(pelouse));
    }

    @Test
    public void executerSequence() {
        positionA = new Position(1,3,Orientation.NORTH);
        tondeuseA.executerSequence("GAGAGAGAA");
        Assert.assertTrue(tondeuseA.getPosition().equals(positionA));
    }
}