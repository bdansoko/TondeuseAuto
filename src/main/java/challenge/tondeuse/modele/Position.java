package challenge.tondeuse.modele;

import java.util.Objects;

/**
 * La position de la tondeuse est représentée par une combinaison de coordonnées (x,y) et d'une
 * lettre indiquant l'orientation selon la notation cardinale anglaise (N,E,W,S)
 * @author bdansoko
 *
 */
public class Position {

	/**
	 * Coordonnées de la tondeuse
	 */
	private Coordonnees coordonnees;
	/**
	 * Orientation de la tondeuse
	 */
	private Orientation orientation;
	
	/**
	 * Constructeur de classe
	 * @param x : int
	 * @param y : int
	 * @param orientation : Orientation
	 */
	public Position(int x, int y, Orientation orientation) {
		this.coordonnees = new Coordonnees(x, y);
		this.orientation = orientation;
	}

    /**
     * Constructeur de classe
     * @param x : int
     * @param y : int
     * @param orientation : String
     */
    public Position(int x, int y, String orientation) {
        this.coordonnees = new Coordonnees(x, y);
	    for (Orientation o :Orientation.values()){
	        if (o.toString().equals(orientation)){
                this.orientation = o;
            }
        }
    }

	/**
	 * Constructeur
	 * @param c : Cordonnees
	 * @param orientation : Orientation
	 */
	public Position(Coordonnees c , Orientation orientation) {
		this.coordonnees = c;
		this.orientation = orientation;
	}
	
	/**
	 * retourne les coordonnées de la tondeuse
	 * @return Coordonnees
	 */
	public Coordonnees getCoordonnees() {
		return coordonnees;
	}

	/**
	 * Modifie la position de la tondeuse
	 * @param x : int
	 * @param y : int
	 */
	public void setCoordonnees(int x, int y) {
		this.coordonnees = new Coordonnees(x, y);
	}
	
	/**
	 * Modifie la position de la tondeuse
	 * @param i : int
	 */
	public void setCoordonneeX(int i) {
		this.coordonnees.setX(coordonnees.getX() + i);
	}

	/**
	 * Modifie la position de la tondeuse
	 * @param i : int
	 */
	public void setCoordonneeY(int i) {
		coordonnees.setY(coordonnees.getY() + i);
	}
	/**
	 * Retourne l'orientation de la tondeuse
	 * @return Orientation
	 */
	public Orientation getOrientation() {
		return orientation;
	}

	/**
	 * Modifie l'orientation de tondeuse
	 * @param orientation : Orientation
	 */
	public void setOrientation(Orientation orientation) {
		this.orientation = orientation;
	}

	/**
	 * Permet d'afficher la position de la tondeuse
	 */
	public String toString() {
		return coordonnees+" "+orientation.toString();
	}

	public boolean equals(Object p){
		if (p instanceof Position) {
			return coordonnees.equals(((Position) p).coordonnees) && orientation.equals(((Position) p).orientation);
		}else
			return false;
	}

	@Override
	public int hashCode() {

		return Objects.hash(coordonnees, orientation);
	}
}
