package cucumber.steps;

import challenge.surface.modele.Surface;
import challenge.surface.modele.SurfaceRectangulaire;
import challenge.tondeuse.modele.*;
import challenge.utils.Constantes;
import cucumber.api.PendingException;
import cucumber.api.java.fr.Soit;
import cucumber.api.java.fr.Et;
import cucumber.api.java.fr.Lorsque;
import cucumber.api.java.fr.Alors;
import org.junit.Assert;
import org.apache.log4j.Logger;

public class TondeuseSteps {
	final static Logger logger = Logger.getLogger(TondeuseSteps.class);


	Surface pelouse;
	Position position;
	Tondeuse tondeuse;

	@Soit("^\"([^\"]*)\" les coordonnées du coin superieur de la pelouse$")
	public void fixCordonneesPelouse(String arg1) throws Throwable {
	    pelouse = null;
        position = null;
        Tondeuse.getListTondeuses().clear();
        tondeuse = new TondeuseGazon();
	    logger.debug("Exécution de la step : @Soit \""+ arg1 + "\" les coordonnées du coin superieur de la pelouse");
		arg1 = arg1.trim();
		String[] s = arg1.split(" ");
		if(s.length == 2 ){
			int x = Integer.parseInt(s[0]);
			int y = Integer.parseInt(s[1]);
			pelouse = new SurfaceRectangulaire(x,y);
            tondeuse.setPelouse(pelouse);
            System.out.println(tondeuse);
		}else {
			logger.debug("Paramètre invalide. Exemple de paramètre valide : \"5 5\"");
			throw new PendingException();
		}
	}

	@Et("^\"([^\"]*)\" la position initiale de la tondeuse ainsi que son orientation$")
	public void fixPositionInitiale(String arg1) throws Throwable {
		logger.debug("Exécution de la step : @Et \""+ arg1 + "\" la position initiale de la tondeuse ainsi que son orientation");
		arg1 = arg1.trim();
		String[] s = arg1.split(" ");;
		if(s.length == 3 ){
			int x = Integer.parseInt(s[0]);
			int y = Integer.parseInt(s[1]);
			switch (s[2]){
				case Constantes.NORTH:
					position = new Position(x, y, Orientation.NORTH);
					break;
				case Constantes.EAST:
					position = new Position(x, y, Orientation.EAST);
					break;
				case Constantes.WEST:
					position = new Position(x, y, Orientation.WEST);
					break;
				case Constantes.SOUTH:
					position = new Position(x, y, Orientation.SOUTH);
					break;
			}
            tondeuse.setPosition(position);
		}else {
			logger.debug("Paramètre invalide. Exemple de paramètre valide : \"1 2 E\"");
			throw new PendingException();
		}
	}

	@Lorsque("^j'execute la sequence suivante: \"([^\"]*)\"$")
	public void executerSequenceSuivante(String arg1) throws Throwable {
		logger.debug("Exécution de la step : @Lorsque j'execute la sequence suivante: \""+ arg1+"\"");
		tondeuse.executerSequence(arg1.toString());
	}

    @Alors("^j'obtiens comme resutat\\(position finale de la tondeuse\\): \"([^\"]*)\"$")
	public void compare(String arg1) throws Throwable {
		logger.debug("Exécution de la step : @Alors j'obtiens comme resutat(position finale de la tondeuse): \""+ arg1+"\"");
		Assert.assertTrue(arg1.equals(tondeuse.getPosition().toString()));
	}
}
