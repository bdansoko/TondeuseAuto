# language: fr
@wip
Fonctionnalité: Test tondeuse
  Scénario: nominal 1
    Soit "5 5" les coordonnées du coin superieur de la pelouse
    Et "1 2 N" la position initiale de la tondeuse ainsi que son orientation
    Lorsque j'execute la sequence suivante: "GAGAGAGAA"
    Alors j'obtiens comme resutat(position finale de la tondeuse): "1 3 N"

  Scénario: nominal 2
    Soit "5 5" les coordonnées du coin superieur de la pelouse
    Et "3 3 E" la position initiale de la tondeuse ainsi que son orientation
    Lorsque j'execute la sequence suivante: "AADAADADDA"
    Alors j'obtiens comme resutat(position finale de la tondeuse): "5 1 E"

  Scénario: la position après mouvement est en dehors de la pelouse
    Soit "5 5" les coordonnées du coin superieur de la pelouse
    Et "1 5 N" la position initiale de la tondeuse ainsi que son orientation
    Lorsque j'execute la sequence suivante: "AAAAAAA"
    Alors j'obtiens comme resutat(position finale de la tondeuse): "1 5 N"
    Et "5 1 E" la position initiale de la tondeuse ainsi que son orientation
    Lorsque j'execute la sequence suivante: "AAAAAAA"
    Alors j'obtiens comme resutat(position finale de la tondeuse): "5 1 E"